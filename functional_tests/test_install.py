#!/usr/bin/env python                                                            
# -*- coding: UTF-8 -*-
import pytest
import devimpy
from devimpy import git, utils, create


@pytest.fixture(scope="class")
def parser():
    return devimpy.create_parser()

@pytest.fixture(scope="function")
def env(tmpdir):
    env = {}
    env['home'] = str(tmpdir.mkdir("homedir"))
    return env


# Dev runs devimpy --create
class TestCreate:
    p = parser()
    p.parse_args(["--create"])
    u = utils.Utils()

    # Dev has not got git installed 
    def test_when_git_is_not_installed(self, mocker):
        mocker.patch.object(git.Git, "has_git")
        g = git.Git(self.u)
        g.has_git.return_value = False
        err = create.Create(g).execute()
        assert err > 0


    # Dev has git installed
    def test_when_git_is_installed(self, mocker):
        mocker.patch.object(git.Git, "has_git")
        g = git.Git(self.u)
        g.has_git.return_value = True
        err = create.Create(g).execute()
        assert err == 0

    # Dev has no User .gitconfig
    # and so is asked for name
    # and then email address
    def test_no_gitconfig(self, mocker, capsys):
       mocker.patch.object(git.Git, "read_config")
       g = git.Git(self.u)
       g.has_git.return_value = True
       g.read_config.return_value = False
       create.Create(g).execute()
       out, err = capsys.readouterr()
       #assert out == "Please enter your name:"



    # Dev is show the current settings for git name and email
    def test_read_user_git_config(self, monkeypatch, mocker, env):
        mocker.patch.object(git.Git, "read_config")
        g = git.Git(self.u)
        g.has_git.return_value = True
        path = env['homedir'].join(".gitconf")

        content = """[user]
            email = dev@example.co.uk
            name = Dev Eloper
            """
        path.write(content)
        monkeypatch.setattr(g, "read_config", lambda x: path.read())
#        assert "Is the following correct? [Y/n]" in g.read_config()


    # and is asked if the settings are correct for this project
         

# Dev chooses No
# Dev is asked to make changes to the name
# Dev is offered the current global setting
# Dev accepts the current setting by pressing <ENTER>
# Dev is asked to make changes to the email
# Dev is offered the current setting
# Dev type in a new email and presses <ENTER>
# Dev is asked if the settings are correct for this project
# Dev chooses Yes
# Dev checks that the new settings have been written to the project git config
