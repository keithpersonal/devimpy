# -*- coding: UTF-8 -*-

import subprocess

##############################################################################
class Utils:
    """
    Contains various functions that are used by DeVimPy
    """

    # -----------------------------------------------------------------------
    def __init__(self):
        pass

    # ------------------------------------------------------------------------
    def run_command(self, command):
        """
        run a OS command from a list of strings.
        Returns True if the exit code of the command was 0
        or else False
        This is basically a wrapper for subprocess.
        """
        try:
            output = subprocess.check_output(command)
        except FileNotFoundError:
            return False
        return output


