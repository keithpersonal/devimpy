# -*- coding: UTF-8 -*-                                                        
import pytest
from devimpy import utils

@pytest.fixture(scope="function")
def util():
    return utils.Utils()

def test_run_command():
    u = util()
    t = u.run_command(["not_a_real_command"])
    assert t == False

    # makes the assumption that cat is installed on every system, is this bad?
    t = u.run_command(["cat", "--version"])
    print(type(t))
    assert isinstance(t, bytes)

