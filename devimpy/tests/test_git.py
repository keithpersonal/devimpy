# -*- coding: UTF-8 -*-                                                        
import pytest
from devimpy import utils, git

@pytest.fixture(scope="function")
def util():
    return utils.Utils()

@pytest.fixture(scope="module")
def gits(env, util):
    return git.Git(env, util)

def test_has_git_when_not_installed(monkeypatch):
    u = util()
    env = None
    monkeypatch.setattr(u, "run_command", lambda x: False)
    g = gits(env, u)
    rtn = g.has_git()
    assert rtn == False


def test_has_git_when_is_installed(monkeypatch):
    u = util()
    env = None
    monkeypatch.setattr(u, "run_command", lambda x: bytes())
    g = gits(env, u)
    rtn = g.has_git()
    rtn == True

def test_get_user_config_no_config_file(tmpdir, monkeypatch, capsys):
    u = util()
    env = {'homedir': str(tmpdir.mkdir("tmphome"))}
    g = gits(env, u)
    monkeypatch.setitem(__builtins__, "input", lambda x: "Dev Eloper")
    g.get_user_config()
    out, err = capsys.readouterr()
    assert "What is your name?" in out
#    assert g.name == "Dev Eloper"


#def test_get_user_config_with_config_file(tmpdir, capsys):
#    u = util()
#    tempdir = tmpdir.mkdir("tmphome")
#    env = {'homedir': str(tempdir)}
#    f = tempdir.join(".gitconfig")
#    content = """[user]
#    email = dev@example.co.uk
#    name = Dev Eloper
#    """
#    f.write(content)
#    g = gits(env, u)
#    g.get_user_config()
#    out, err = capsys.readouterr()
#    assert "email: dev@example.co.uk" in out
#    assert "name: Dev Eloper" in out
#    assert "Is this correct for the project? [Y/n]: y" in out



