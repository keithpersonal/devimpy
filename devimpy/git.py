# -*- coding: UTF-8 -*-                                                        

import os

############################################################################## 
class Git:
    """
    Contains various functions to handle git
    """

    # -----------------------------------------------------------------------  
    def __init__(self, env, utils):
        self.env = env
        self.utils = utils

        self.name = ""
        self.email = ""

    # ------------------------------------------------------------------------ 
    def has_git(self):
        """
        Checks to see if git is installed by requesting git's version.
        Returns True if found.
        """
        r = self.utils.run_command(["git", "--version"])
        if r:
            rtn = True
        else:
            print("git is required but has not been found.")
            rtn = False
        return rtn


    # ------------------------------------------------------------------------
    def get_user_config(self):
        """
        """
        conFile = os.path.join(self.env['homedir'], ".gitconfig")
        if os.path.exists(conFile):
            pass
        else:
            self.name = input("What is your name?")
