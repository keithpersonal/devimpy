# -*- coding: UTF-8 -*- 

class Create():
    def __init__(self, env, utils, git):
        self.env = env
        self.git = git
        self.utils = utils

    # ------------------------------------------------------------------------
    def execute(self):
        """
        Makes an attempt to create a new project.
        """
        err = 0

        # Do not create the project without git
        if not self.git.has_git():
            return 1

        gitConf = self.git.get_user_config()

        return err
