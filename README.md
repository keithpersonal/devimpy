# DeVimPy #

* __Dev__eloping with __Vim__ for __Py__thon.

### What is DeVimPy for? ###

* Managing Python Projects in a command line only environment.

### Why DeVimPy? ###

* Currently, DeVimPy is a project to aid my personal learning in using various Pythonic related development techniques. Specifically Test Driven Development (TDD) but also all aspects of discovering, what seems to be somewhat subjective, Development Best Practices.

* One day it may become useful.

* Version
0.0.dev1


### How do I get set up? ###

*  pip install git@bitbucket.org:keithpersonal/devimpy.git

* Configuration

* Dependencies

* Database configuration

* How to run tests

* Deployment instructions

### Contribution guidelines ###

* Writing tests

Unknow as of yet

* Code review

Unknow as of yet

* Other guidelines

Unknow as of yet

### Who do I talk to? ###

* Keith Lee <code@keithlee.co.uk>

* Other community or team contact - None