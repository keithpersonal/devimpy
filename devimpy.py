#!/usr/bin/env python                                                            
# -*- coding: UTF-8 -*- 
import os
import sys
import argparse
from collections import OrderedDict

from devimpy.utils import Utils
from devimpy.git import Git

#from vim import Vim
from devimpy import create
#from clone import Clone
#from develop import Develop


# ----------------------------------------------------------------------------
def main(env, args):
    """"""

    err = 0
    utilities = Utils()
    git = Git(env, utilities)
    # OrderedDict is required so that the user's options
    # are processed in the correct order
    # ie you cannot start work developing before a project has been created
    methods = OrderedDict()
 #   methods['vim'] = Vim
    methods['create'] = create.Create
 #   methods['clone'] =  Clone
 #   methods['dev'] = Develop

    # This is a factory for spawning the objects that handle
    # each option
    for op in methods:
        if args[op]:
            ob = methods[op](env, utilities, git)
            err = ob.execute()

            # We have had some problem so stop processing the options
            if err > 0:
                break

    sys.exit(err)

# ----------------------------------------------------------------------------
def create_parser():
    """"""
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-c", "--create",
                        action="store_true",
                        help="Create a new project in you repository.")

    group.add_argument("-l", "--clone",
                        action="store_true",
                        help="Clone project from a remote repository.")

    parser.add_argument("-d", "--dev",
                        action="store_true",
                        help="Develop a project in your repository.")

    parser.add_argument("-v", "--vim",
                         action="store_true",
                         help="Configure vim to develop Python")

    return parser


# ----------------------------------------------------------------------------
def set_environment():
    """"""
    env = {}
    env['homedir'] = os.path.expanduser("~")

    return env


# ----------------------------------------------------------------------------
if __name__ == '__main__':

    parser = create_parser()
    args = dict(parser.parse_args()._get_kwargs())
    env = set_environment()
    main(env, args)

