#!/usr/bin/env python
# -*- coding: UTF-8 -*- 
from setuptools import setup
import re
from io import open
import os

print(os.getcwd())

# get version from version file
with open("version.py", encoding="utf-8") as f:
            __version__ = re.search(
                r'__version__\s*=\s*[\'"]([^\'"]*)[\'"]', f.read()).group(1)

# Get the long description from the README file
with open("README.md", encoding="utf-8") as f:
        long_description = f.read()
setup(
        name='devimpy',
        description='Developing with Vim for Python',
        long_description=long_description,
        author='Keith Lee',
        author_email='code@keithlee.co.uk',
        version=__version__,
        url='https://bitbucket.org/keithpersonal/devimpy',
        packages=['devimpy'],

        setup_requires=['pytest-runner'],
        tests_require=['pytest', 'pexpect', 'pytest-mock'],
        # install_requires=["configobj"],

        # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
        classifiers=[
            # How mature is this project? Common values are
            #   3 - Alpha
            #   4 - Beta
            #   5 - Production/Stable
            "Development Status :: 3 - Alpha",
            # Indicate who your project is intended for
            "Intended Audience :: Developers",
            "Topic :: Software Development :: Build Tools",
            # Specify the Python versions you support here.
            # In particular, ensure
            # that you indicate whether you support Python 2,
            # Python 3 or both.
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3",
                                            ],
        keywords="setuptools development git",


    )
